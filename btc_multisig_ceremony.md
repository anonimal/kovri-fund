# Bitcoin Multisig Address Generation Ceremony

## Considerations
It is not particularly difficult to create the multisig address, the
complexity arises most obviously when trying to spend from one. We can
reference the previous btc_ltc_multisig.md document for details on that.

All that is needed to synthesize the multisig address is the pubkeys of
all 3 individual bitcoin addresses. We will each generate our own, normal,
bitcoin addresses using whatever method is safe and familiar to us. Any
method is fine, with the caveat that you must somehow be able to sign a 
hex blob with your bitcoin secret key.

Doing that is straightforward with the core bitcoin software, less so with
hardware wallets. Because I am confident I can keep my private key safe
and don't want to have to deal with a hardware wallet interface in order
to do signing, I am going to generate my address using the process below.

(**NOTE** You do not need to have a copy of the blockchain or even be connected to the
internet to generate a bitcoin address and secret key as long as you have
`bitcoind` + `bitcoin-cli` or `Bitcoin-Qt` previously installed.)

## Step 1 - Install Software
Install `bitcoind` and `bitcoin-cli` OR `Bitcoin-Qt`. Disconnect from the internet.

## Step 2 - Start RPC Server
To start `bitcoind` or `Bitcoin-Qt` in "offline" mode, run it will these arguments.
We are using a disposable wallet file and not connecting to anything:

`bitcoind -maxconnections=0 -bind=127.0.0.1 -discover=0 -onlynet=ipv4 -dns=0 -dnsseed=0 -externalip=127.0.0.1 -listenonion=0 -printtoconsole=1 -daemon=0 -server=1 -rest=1 -rpcbind=127.0.0.1:8332 -rpcallowip=127.0.0.1 -rpcport=8332 -rpcuser=user -rpcpassword=pass -wallet=/tmp/wallet.dat -datadir=/tmp -conf=/dev/null`

## Step 3 - Generate Address
Now we can generate an address and dump it's private key with `bitcoin-cli`.
 - Generate a new address
 - Dump it's info
 - Dump it's privkey

### Shortcut for argumetns
`alias bitcoin-cli="bitcoin-cli -rpcuser=user -rpcpassword=pass -rpcport=8332 -rpcconnect=127.0.0.1"`

### For more info
`bitcoin-cli help`

### Generate new address
`bitcoin-cli getnewaddress`

### Get pubkey (will be pubkey key in json object)
`bitcoin-cli getaddressinfo <address>`

### Dump private key
`bitcoin-cli dumpprivkey <address>`


## Step 4 - Save this info
Keep it secret. Keep it safe. But we will then exchange our bitcoin pubkeys.
At least one of us has to then run the command `createmultisig` on all 3 pubkeys.
We can all do this and the outcome should be the same address. The arguments are
the number of signers required and a json array of pubkeys:

`bitcoin-cli createmultisig 2 '["<66_hex_chars_pubkey>", "<66_hex_chars_pubkey>", "<66_hex_chars_pubkey>"]'`

This should output a valid address where BTC can be sent, and the redeem script, which is what will have to be
signed by 2 of 3 keyholders and incorported into a rawtransaction as per the previous write up.


## TL;DNR
 - Generate a Bitcoin keypair and share the 66 character hex pubkey of that address whatever it is.
 - Make sure you keep the private key safe and can sign messages with it.
