# Monero M/N HOWTO

- Intended for use with `monero-wallet-cli`
- For full options, on the commandline, use `monero-wallet-cli --help` or type `help` from within the wallet readline environment
- The type of secure side-channel used is decided by the party involved. For these examples, we will use GPG from the commandline

## 1. Create and prepare the shared wallet
1. Alice, Bob, and Charlie all create a new empty wallet
   - `$ monero-wallet-cli`
   - follow the instructions from within the wallet environment
2. They are now 'within' the wallet environment, so they run the following command:
   - `prepare_multisig`
3. They then send their output to each other from the shell via secure sidechannels
   - Save the string (the `Multisig...` string that comes from the `prepare_multisig` command) to `tweedle_dee.txt`
   - `$ gpg -r alice@email -r bob@email -r charlie@email -e tweedle_dee.txt`
4. After receiving everyone else's encrypted file, they decrypt from the shell and then run `make_multisig` from within their wallet - pasting in the strings that they received (note: we pick 2 for threshold for 2/3 multisig)
   - `$ gpg -d tweedle_dee.txt && cat tweedle_dee.txt`
   - `make_multisig 2 Multisig1... Multisig2...`
5. They then send this **new** output to each other from the shell via secure sidechannels
   - Save the **new** string (the `Multisig...` string that comes from the `make_multisig` command) to `tweedle_dum.txt`
   - `$ gpg -r alice@email -r bob@email -r charlie@email -e tweedle_dum.txt`
6. After receiving everyone else's **new** encrypted file, they decrypt from the shell and then run `exchange_multisig_keys` from within their wallet - pasting in the strings that they received:
   - `$ gpg -d tweedle_dum.txt && cat tweedle_dum.txt`
   - `exchange_multisig_keys Multisig1... Multisig2...`
7. E'finito

## 2. Receive funds

1. A donor sends funds to this new shared wallet address
2. Alice, Bob, and Charlie refresh their shared wallet from within the wallet environment
   - `refresh`
3. They all run `export_multisig_info`
   - Alice runs `export_multisig_info alice.info`
   - Bob runs `export_multisig_info bob.info`
   - Charlie runs `export_multisig_info charlie.info`
4. They send these files to each other via a secure sidechannel
5. They all run `import_multisig_info` and import the other's respective files
   - Alice runs `import_multisig_info bob.info charlie.info`
   - Bob runs `import_multisig_info alice.info charlie.info`
   - Charlie runs `import_multisig_info alice.info bob.info`
6. They all run `refresh`
7. E'finito

Repeat `export_multisig_info` and `import_multisig_info` as needed.

## 3. Send funds

1. Perform a `transfer` from within the wallet environment from any of the signers to a monero address
   - `transfer normal 11 4LongMoneroAddress 0.01`
2. An unsigned TX will be written to a file in the directory that you started the wallet from
   - by default, this file will be titled `multisig_monero_tx`
3. Send this file to at least 1 of the other 2 signers
4. Any 1 of those 2 signers then moves the `multisig_monero_tx` file into the directory that they started the wallet from
5. They sign the transaction from within the wallet environment
   - `sign_multisig multisig_monero_tx`
6. The person who signed now submits to the network from within the wallet environment
   - `submit_multisig multisig_monero_tx`
7. E'finito

Repeat `export_multisig_info` and `import_multisig_info` as needed.
